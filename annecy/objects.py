"""
This file contains code that hides a bit of raw strings in JSON data behind clean properties to
help manipulate the corresponding objects.
"""

from typing import Dict, Any, Tuple


class Category:

    @property
    def identifier(self) -> str:
        return self._identifier

    @property
    def label(self) -> str:
        return self._label

    def __init__(self, identifier: str, label: str):
        self._identifier = identifier
        self._label = label

    @classmethod
    def from_json(cls, identifier: str, json_data: Dict[str, Any]):
        return Category(
            identifier,
            json_data['fr'],
        )


class FestivalEntry:

    @property
    def identifier(self) -> str:
        return self._identifier

    @property
    def title(self) -> str:
        return self._title

    @property
    def description(self) -> str:
        return self._description

    @property
    def category(self) -> Category:
        return self._category

    @category.setter
    def category(self, value: Category):
        self._category = value

    def __init__(self, identifier: str, title: str, description: str, category: Category):
        self._identifier = identifier
        self._title = title
        self._description = description
        self._category = category

    @classmethod
    def from_json(cls, json_data: Dict[str, Any]):
        category_id = json_data['film_code_categ']
        return FestivalEntry(
            json_data['film_id'],
            json_data['film_titre_fr'] or json_data['film_titre_original'],
            json_data['film_synopsis_fr'],
            Category.from_json(category_id, json_data['film_categories'][category_id]),
        )


class Screening:

    @property
    def identifier(self) -> str:
        return self._identifier

    @property
    def title(self) -> str:
        return self._title

    @property
    def start_date(self) -> Tuple[int, int, int]:
        """ Start date in (YYYY, MM, DD) format """
        return self._start_date

    @property
    def end_date(self) -> Tuple[int, int, int]:
        """ End date in (YYYY, MM, DD) format """
        return self._end_date

    @property
    def start_time(self) -> Tuple[int, int]:
        """ Start time in (HH, MM) format """
        return self._start_time

    @property
    def end_time(self) -> Tuple[int, int]:
        """ End time in (HH, MM) format """
        return self._end_time

    @property
    def location(self) -> str:
        """ Identifier of the screening location/cinema """
        return self._location

    @property
    def room(self) -> str:
        """ Identifier of the screening room within its location """
        return self._room

    def __init__(
            self, identifier: str, title: str, start_date: Tuple[int, int, int], end_date: Tuple[int, int, int],
            start_time: Tuple[int, int], end_time: Tuple[int, int], location: str, room: str
    ):
        self._identifier = identifier
        self._title = title
        self._start_date = start_date
        self._end_date = end_date
        self._start_time = start_time
        self._end_time = end_time
        self._location = location
        self._room = room

    @classmethod
    def from_internal_json(cls, identifier: str, json_data: Dict[str, Any]):
        return cls.from_raw_json_fields(
            identifier,
            json_data['title'],
            json_data['start_date'],
            json_data['end_date'],
            json_data['start_time'],
            json_data['end_time'],
            json_data['location'],
            json_data['room'],
        )

    @classmethod
    def from_raw_json_fields(
            cls, identifier: str, title: str, start_date: str, end_date: str, start_time: str, end_time: str,
            location: str, room: str
    ):
        # noinspection PyTypeChecker
        return Screening(
            identifier,
            title,
            tuple(int(x) for x in start_date.split('-')),
            tuple(int(x) for x in end_date.split('-')),
            tuple(int(x) for x in start_time.split(':')),
            tuple(int(x) for x in end_time.split(':')),
            location,
            room,
        )

    def get_date_text(self):
        return 'le ' + str(self._start_date[2])

    def get_datetime_text(self):
        return self.get_date_text() + f' à {self.start_time[0]}h{self.start_time[1]:02}'
