import time
from dataclasses import dataclass
from typing import Tuple, Set, Dict, Optional, List

from .datastore import Datastore
from .download import ApiClient
from .objects import Screening, FestivalEntry


@dataclass
class WishList:
    # The set of acceptable locations to go
    allowed_locations: Set[str]

    # Identifiers of films which you would like to book from most to least desired
    wishes: Tuple[str]


def search_reservations(
        wishlist: WishList, cooldown_in_seconds: int, do_booking: bool, api_client: ApiClient, data: Datastore):
    if cooldown_in_seconds <= 0:
        raise ValueError('Le temps entre deux recherches doit ere un nombre de secondes strictement positif')

    while True:
        _do_search_reservations_attempt(wishlist, do_booking, api_client, data)
        time.sleep(cooldown_in_seconds)


def _do_search_reservations_attempt(wishlist: WishList, do_booking: bool, api_client: ApiClient, data: Datastore):
    print('----------' * 5)
    print('Démarrage d\'une recherche d\'échanges de réservations...')
    slots_per_day = api_client.configuration.booking_per_days
    available_slots = {
        (2023, 6, d): slots_per_day
        for d in range(11, 18)
    }

    current_reservations = [
        Screening.from_raw_json_fields(
            s['plng_unique_id'],
            s['rdv_nom_fr'],
            s['plng_date_debut'],
            s['plng_date_fin'],
            s['plng_heure_debut'],
            s['plng_heure_fin'],
            s['lieu']['lieu_id'],
            s['lieu']['salle_id'],
        )
        for s in api_client.get_reserved_screenings()]

    # Count how many remaining slots we have
    for reservation in current_reservations:
        # Never ever trust user data
        if reservation.start_date != reservation.end_date:
            print('Les événements sur plusieurs jours ne sont pas supportés. Des bugs pourraient survenir.')
            continue
        if reservation.start_date not in available_slots:
            print(f'Une des réservations existantes (pour la projection {reservation.identifier}) '
                  f'semble exister hors de la période du festival')
            continue
        available_slots[reservation.start_date] -= 1

    event_to_films = {e: f for (e, f) in data.films_to_events.items()}
    entries_index: Dict[str, FestivalEntry] = {e.identifier: e for e in data.entries}

    for index, desired_film_id in enumerate(wishlist.wishes):
        desired_film = entries_index[desired_film_id]
        desired_film_name = desired_film.title
        priority = len(wishlist.wishes) - index
        has_solution, screenings_to_cancel, screening_to_book = _try_find_trade(desired_film_id, priority)
        if has_solution:

            print(f'Un échange a été trouvé qui permet de voir {desired_film_name} :-) !', end='')
            if screenings_to_cancel:
                print(f' Il peut être réservé à condition d\'annuler la ou les réservations suivantes ' +
                      screening_to_book.get_date_text() + ' :')
                print('\n'.join(f'\t- {s.title}' for s in screenings_to_cancel))
            else:
                print(f' Il peut être réservé {screening_to_book.get_datetime_text()}')

            if do_booking:
                if not (
                    all(api_client.cancel_reservation(r.identifier) for r in screenings_to_cancel)
                    and api_client.request_reservation(screening_to_book.identifier)
                ):
                    print(f'Echec lors de la réalisation de l\'échange pour le film {desired_film_name} ' +
                          screening_to_book.get_datetime_text())

    print('La recherche d\'échanges est terminée')


def _try_find_trade(desired_film_id, priority) -> Tuple[bool, Optional[List[Screening]], Optional[Screening]]:
    return (
        True,
        [
            Screening("123456", "Projection à annuler 1", (2023, 6, 15), (2023, 6, 15), (9, 0), (10, 20), "1", "5"),
            Screening("548978", "Projection à annuler 2", (2023, 6, 15), (2023, 6, 15), (10, 10), (11, 20), "1", "5"),
        ],
        Screening("xyetrer", "Projection à réserver", (2023, 6, 15), (2023, 6, 15), (9, 30), (11, 5), "1", "5"),
    )

def _get_future_screenings
    time.time()
    for