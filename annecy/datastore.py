import json
from pathlib import Path
from typing import List, Dict

from .download import ApiClient, download_categories, download_entries, download_planification_data
from .objects import FestivalEntry, Category, Screening


class Datastore:

    @property
    def categories(self) -> Dict[str, Category]:
        if self._categories is None:
            if not self._categories_path.is_file():
                self._make_cache_dir()
                download_categories(self._api_client, self._categories_path)

            self._categories = {
                k: Category(k, v)
                for (k, v) in self._load_json(self._categories_path).items()}
        return self._categories

    @property
    def entries(self) -> List[FestivalEntry]:
        if self._entries is None:
            if not self._entries_path.is_file():
                self._make_cache_dir()
                download_entries(self._api_client, self._entries_path)

            self._entries = [
                FestivalEntry.from_json(e)
                for e in self._load_json(self._entries_path)]
            # Alias categories to make comparisons easier
            categories = self.categories
            for entry in self._entries:
                entry.category = categories[entry.category.identifier]
        return self._entries

    @property
    def films_to_events(self) -> Dict[str, str]:
        if self._films_to_events is None:
            if not self._films_to_events_path.is_file():
                self._make_cache_dir()
                self._download_planification_data()
            self._films_to_events = self._load_json(self._films_to_events_path)
        return self._films_to_events

    @property
    def events_to_screenings(self) -> Dict[str, Screening]:
        if self._events_to_screenings is None:
            if not self._events_to_screenings_path.is_file():
                self._make_cache_dir()
                self._download_planification_data()
            self._events_to_screenings = {
                screening_id: Screening.from_internal_json(screening_id, s)
                for (screening_id, s) in self._load_json(self._events_to_screenings_path).items()
            }
        return self._events_to_screenings

    @property
    def locations(self) -> Dict[str, str]:
        if self._locations is None:
            if not self._locations_path.is_file():
                self._make_cache_dir()
                self._download_planification_data()
            self._locations = self._load_json(self._locations_path)
        return self._locations

    @property
    def rooms(self) -> Dict[str, str]:
        if self._rooms is None:
            if not self._rooms_path.is_file():
                self._make_cache_dir()
                self._download_planification_data()
            self._rooms = self._load_json(self._rooms_path)
        return self._rooms

    @property
    def _categories_path(self) -> Path:
        return self._cache_location / 'categories.json'

    @property
    def _entries_path(self) -> Path:
        return self._cache_location / 'entries.json'

    @property
    def _films_to_events_path(self) -> Path:
        return self._cache_location / 'films to events.json'

    @property
    def _events_to_screenings_path(self) -> Path:
        return self._cache_location / 'events to screenings.json'

    @property
    def _locations_path(self) -> Path:
        return self._cache_location / 'locations.json'

    @property
    def _rooms_path(self) -> Path:
        return self._cache_location / 'rooms.json'

    def __init__(self, api_client: ApiClient, cache_location: Path):
        self._api_client = api_client
        self._cache_location = cache_location

        self._categories = None
        self._entries = None
        self._films_to_events = None
        self._events_to_screenings = None
        self._locations = None
        self._rooms = None

    def refresh_cache(self):
        self._make_cache_dir()
        print('* Downloading film categories...')
        download_categories(self._api_client, self._categories_path)
        print('* Downloading festival entries...')
        download_entries(self._api_client, self._entries_path)
        print('* Downloading planning data...')
        self._download_planification_data()

    def _download_planification_data(self):
        download_planification_data(
            self._api_client, self._films_to_events_path, self._events_to_screenings_path,
            self._locations_path, self._rooms_path)

    def _make_cache_dir(self):
        self._cache_location.mkdir(parents=True, exist_ok=True)

    @classmethod
    def _load_json(cls, location: Path):
        with location.open() as fd:
            return json.load(fd)
