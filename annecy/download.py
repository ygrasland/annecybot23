"""
In this file goes code which downloads static information from the festival website.
This information may be put in cache for faster reload.
"""
import hashlib
import json
from collections import defaultdict
from pathlib import Path

import requests
from typing import cast, Dict, Any, Optional, Iterable


class ApiAuthentication:

    @property
    def accredited_id(self) -> int:
        return int(self._json_data['acc_id'])

    @property
    def person_id(self) -> int:
        return int(self._json_data['pers_id'])

    @property
    def first_name(self) -> str:
        return self._json_data['pers_prenom']

    @property
    def last_name(self) -> str:
        return self._json_data['pers_nom']

    @property
    def accreditation_category(self) -> int:
        return int(self._json_data['acc_bureau'])

    @property
    def api_token(self) -> str:
        return self._json_data['pers_cle_connexion']

    def __init__(self, json_data: Dict[str, Any]):

        # Check connection success
        state = json_data['etat']
        if state['cle_valid'] is not True or state['connexion'] != 1:
            raise ValueError('Connection failure')

        # Get useful information
        self._json_data = cast(Dict[str, str], json_data['donnees'])


class ApiResponse:

    @property
    def json_data(self) -> Dict[str, Any]:
        return self._json_data

    def __init__(self, json_data: Dict[str, Any]):
        self._json_data = json_data['donnees']


class ApiConfiguration(ApiResponse):

    @property
    def booking_per_days(self) -> int:
        bookings = cast(Dict[str, int], self._json_data['reservation_per_day_by_desk'])
        return bookings[str(self._authentication.accreditation_category)]

    def __init__(self, json_data: Dict[str, object], authentication: ApiAuthentication):
        super().__init__(json_data)
        self._authentication = authentication


class ApiClient:

    @property
    def username(self) -> str:
        return self._username

    @property
    def password(self) -> str:
        return self._password

    @property
    def environment(self) -> str:
        return self._environment

    @property
    def authentication(self) -> ApiAuthentication:
        self.authenticate()
        return self._authentication

    @property
    def configuration(self) -> ApiConfiguration:
        if self._configuration is None:
            configuration_data = self._get_api_route('obtenir', 'configuration')
            self._configuration = ApiConfiguration(configuration_data, self.authentication)
        return self._configuration

    _key: Optional[str] = None

    def __init__(self, username: str, password: str, environment: str):
        self._username = username
        self._password = password
        self._environment = environment

        self._authentication: Optional[ApiAuthentication] = None
        self._configuration = None

    def authenticate(self, force: bool = False):
        if self._authentication is None or force:
            password_bytes = self._password.encode()
            pwd_hash = hashlib.md5(password_bytes).hexdigest()
            url_suffix = f'obtenir/authentification/{self.username}/{pwd_hash}'
            auth_url = self._make_api_prefix(self._environment) + url_suffix
            raw_authentication_data = self._make_json_get_request(auth_url)
            self._authentication = ApiAuthentication(raw_authentication_data)

    def get_entries(self) -> Iterable:
        return self._get_paginated('obtenir', 'recherche', 'films')

    def get_screenings(self) -> Iterable:
        return self._get_paginated('obtenir', 'recherche', 'planifications')

    def get_reserved_screenings(self) -> Iterable:
        return self._get_paginated('obtenir', 'recherche', 'planifications', 'reservations:true')

    def get_screening_details(self, screening_id: str) -> Dict[str, Any]:
        return self._get_api_route('obtenir', 'planification', screening_id)['donnees']

    def request_reservation(self, screening_id: str) -> bool:
        return self._perform_reservation_action('ajouter', screening_id)

    def cancel_reservation(self, screening_id: str) -> bool:
        return self._perform_reservation_action('supprimer', screening_id)

    def print_api_route(self, *path: str):
        print(json.dumps(self._get_api_route(*path), indent='    '))

    def _get_paginated(self, *path: str) -> Iterable[Any]:
        page = 1
        while True:
            results_parent = self._get_api_route(*path, f'page:{page}')['donnees']
            for e in results_parent['resultats']:
                yield e

            page_count = results_parent['nb_pages']
            current_page = results_parent['page_courante']
            if current_page == page_count:
                break
            page += 1

    def _get_api_route(self, *path: str) -> Dict[str, Any]:
        self.authenticate()
        url_suffix = '/'.join(path)
        url = self._make_api_prefix(self._authentication.api_token) + url_suffix
        return cast(Dict[str, Any], self._make_json_get_request(url))

    def _perform_reservation_action(self, action: str, screening_id: str) -> bool:
        raw_result = self._get_api_route(action, 'reservation', screening_id)
        return raw_result['etat']['statut']

    @classmethod
    def _make_api_prefix(cls, first_path_part: str):
        return f'https://api.annecy.org/{first_path_part}/fr/'

    @classmethod
    def _make_json_get_request(cls, url: str) -> Dict[str, Any]:
        response = requests.get(url)
        response_data = response.json()
        return response_data


def download_entries(client: ApiClient, destination: Path):
    _write_object(list(client.get_entries()), destination)


def download_categories(client: ApiClient, destination: Path):
    categories = {}
    for raw_entry in client.get_entries():
        category_id = raw_entry['film_code_categ']
        category_label = raw_entry['film_categories'][category_id]['fr']
        categories[category_id] = category_label
    with destination.open('w') as fd:
        json.dump(categories, fd, indent=4, ensure_ascii=False)


def download_planification_data(
        client: ApiClient, film_to_event: Path, event_to_screenings: Path, locations: Path, rooms: Path):
    film_to_event_index = {}
    event_to_screenings_index = defaultdict(list)
    locations_index = {}
    rooms_index = {}
    known_events = set()
    for raw_screening in client.get_screenings():
        event_id = raw_screening['rdv_unique_id']
        _extract_screening(raw_screening, event_to_screenings_index[event_id], locations_index, rooms_index)

        # Associate films with events
        if event_id not in known_events:
            screening_id = raw_screening['plng_unique_id']
            detailed_screening = client.get_screening_details(screening_id)

            for film in detailed_screening.get('liste_films', []):
                filmd_id = film['film_id']
                if filmd_id in film_to_event_index and film_to_event_index[filmd_id] != event_id:
                    film_title = film['film_titre_fr'] or film['film_titre_original']
                    print(f'Attention, particularité des données non supportée: le film {filmd_id} ({film_title}) '
                          f'est projeté dans plus d\'un événement du programme !')
                film_to_event_index[filmd_id] = event_id
            known_events.add(event_id)

    _write_object(film_to_event_index, film_to_event)
    _write_object(event_to_screenings_index, event_to_screenings)
    _write_object(locations_index, locations)
    _write_object(rooms_index, rooms)


def _extract_screening(raw_screening_data, event_screenings: list, locations_index, rooms_index):

    # Place (location/cinema + room)
    raw_place = raw_screening_data['lieu']
    location_id = raw_place['lieu_id']
    if location_id not in locations_index:
        locations_index[location_id] = raw_place['lieu_nom']
    room_id = raw_place['salle_id']
    if room_id not in rooms_index:
        rooms_index[room_id] = raw_place['salle_nom_fr']

    event_screenings.append({
        'location': location_id,
        'room': room_id,
        'title': raw_screening_data['rdv_nom_fr'],

        # Time
        'start_date': raw_screening_data['plng_date_debut'],
        'end_date': raw_screening_data['plng_date_fin'],
        'start_time': raw_screening_data['plng_heure_debut'],
        'end_time': raw_screening_data['plng_heure_fin'],
    })


def _write_object(json_data: object, destination: Path):
    with destination.open('w') as fd:
        json.dump(json_data, fd, indent=4, ensure_ascii=False)
