#!/usr/bin/env python3
import json
from argparse import ArgumentParser
from pathlib import Path
import time

from annecy.datastore import Datastore
from annecy.download import ApiClient
from annecy.objects import FestivalEntry
from annecy.reservations import search_reservations, WishList


def create_parser() -> ArgumentParser:
    res = ArgumentParser('Annecy API client')
    res.add_argument(
        '-u', '--user',
        required=True, help='Your user name')
    res.add_argument(
        '-p', '--password',
        required=True, help='Your password')
    res.add_argument(
        '--environment', default='6vybm4ty',
        help='The XXX in https://api.annecy.org/XXX/fr/obtenir/authentification/... - looks like some environment ID.')
    res.add_argument(
        '--cache-path', default=Path() / 'Annecy data cache', type=Path,
        help='Where to cache static data retrieved from the API')

    subparsers = res.add_subparsers(required=True)

    print_parser = subparsers.add_parser(
        'print-entries', help='Print the identifiers and title of all festival entries to help you fill your wishlist')
    print_parser.add_argument('-c', '--categories', nargs='+', default=[])
    print_parser.set_defaults(command=print_entries)

    refresh_parser = subparsers.add_parser(
        'refresh', help='Refresh the static data cache (categories, festival entries, etc.)')
    refresh_parser.set_defaults(command=refresh)

    search_parser = subparsers.add_parser(
        'search',
        help='Hunts for free booking spots for the events of your dreams, and may try to book them for you')
    search_parser.add_argument(
        '-w', '--wishlist', default=Path('whishlist.json'), type=Path,
        help='Path to a JSON file containing the ids of all sessions which you would like to book,'
             'in descending preference order.')
    search_parser.add_argument(
        '-b', '--book', action='store_true', default=False,
        help='Try to apply the discovered reservation trades.')
    search_parser.add_argument(
        '-i', '--interval', default=60, type=int,
        help='Internval in seconds between two search attemps'
    )
    search_parser.set_defaults(command=search)

    return res


def sample_requests(client: ApiClient):
    client.print_api_route(
        'obtenir', 'recherche', 'planifications', 'date:2023-06-11',
        'info_favoris:true', 'reservations:true')
    client.print_api_route('supprimer', 'favori', 'PLNG', '100001504258')

    client.print_api_route('obtenir', 'recherche', 'films', 'info_favoris:true', 'page:2')

    # I guessed this one on my own ^^
    client.print_api_route('ajouter', 'reservation', 'PLNG', '100001504258')


def print_entries(args, _client: ApiClient, store: Datastore):
    categories = args.categories

    def format_entry(entry: FestivalEntry, print_category: bool):
        res = f'{entry.identifier} | {entry.title}\n' \
              f'\tDescription: {entry.description}\n'
        if print_category:
            res += f'\tCategory: {entry.category.identifier} ({entry.category.label})\n'
        return res

    categories = set(categories)
    has_category_filter = bool(categories)
    entries = store.entries
    if has_category_filter:
        entries = filter(lambda e: e.category.identifier in categories, entries)
    entries = sorted(entries, key=lambda e: e.identifier)

    print('\n'.join(
        format_entry(entry, not has_category_filter or len(categories) > 1)
        for entry in entries))


def refresh(_args, _client: ApiClient, store: Datastore):
    print('Downloading static festival program data...')
    start_time = time.time()
    store.refresh_cache()
    end_time = time.time()
    print(f'Refresh complete in {int(end_time - start_time)} seconds')


def search(args, client: ApiClient, store: Datastore):
    whishlist_path: Path = args.wishlist
    if not whishlist_path.is_file():
        raise ValueError(f'The wishlist file at {whishlist_path} does not exist')
    with whishlist_path.open() as fd:
        raw_wishlist = json.load(fd)
        wishlist = WishList(
            set(raw_wishlist['allowed_locations']),
            tuple(raw_wishlist['wishes']))
    search_reservations(wishlist, args.interval, args.book, client, store)


def main():
    args = create_parser().parse_args()
    client = ApiClient(args.user, args.password, args.environment)

    client.authenticate()
    print(f'Authentication complete for {client.authentication.first_name} {client.authentication.last_name}. '
          f'You may make {client.configuration.booking_per_days} booking(s) per day.')

    store = Datastore(client, args.cache_path)
    args.command(args, client, store)


if __name__ == '__main__':
    main()
